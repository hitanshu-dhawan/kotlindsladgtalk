package com.hitanshudhawan.kotlin_dsl_adg_talk

import com.hitanshudhawan.kotlin_dsl_adg_talk.person.Person
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month


val human1 = person {
    firstName = "John"
    lastName = "Wick"
    dateOfBirth = 7 Oct 1996 at 4..15
    children {
        person {
            firstName = "..."
        }
        person {
            firstName = "..."
            lastName = "..."
            children {
                person {
                    //...
                }
                person {
                    //...
                }
            }
        }
    }
}

fun person(block: PersonBuilder.() -> Unit): Person {
    val personBuilder = PersonBuilder()
    personBuilder.block()
    return personBuilder.build()
}

class PersonBuilder {
    var firstName = ""
    var lastName = ""
    var dateOfBirth = LocalDateTime.now()
    var children = ArrayList<Person>()

    infix fun Int.Oct(year: Int): LocalDate {
        return LocalDate.of(this, Month.OCTOBER, year)
    }

    infix fun LocalDate.at(time: IntRange): LocalDateTime {
        return this.atTime(time.first, time.last)
    }

    fun children(block: ChildrenBuilder.() -> Unit) {
        children = ChildrenBuilder().apply(block).children
    }

    fun build(): Person {
        return Person(firstName, lastName, dateOfBirth)
    }
}

class ChildrenBuilder {
    val children = ArrayList<Person>()

    fun person(block: PersonBuilder.() -> Unit) {
        children.add(PersonBuilder().apply(block).build())
    }
}