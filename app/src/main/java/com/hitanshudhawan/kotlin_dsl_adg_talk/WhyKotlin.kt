package com.hitanshudhawan.kotlin_dsl_adg_talk



// 1. Infix functions

class Number(private val n: Int) {

    infix fun add(number: Number): Number = Number(n + number.n)

}

val number1 = Number(10)
val number2 = Number(20)

val number3 = number1 add number2



// 2. Extension functions

fun String.greet(): String {
    return "Hello $this"
}

val greeting = "John".greet()  // "Hello John"



// 3. Lambda as a last parameter

fun repeatAction(action: (Int) -> Unit) {
    for (index in 0 until 5) {
        action(index)
    }
}

fun function3() {

    repeatAction {
        print("index : $it")
    }

}



// 4. Lambda with a receiver

data class Location(var lat: Double = 0.0, var lng: Double = 0.0)

fun location(block: Location.() -> Unit): Location {
    val location = Location()
    location.block()
    return location
}

fun function4() {

    val loc = location {
        lat = 1.1
        lng = 2.2
    }

}
