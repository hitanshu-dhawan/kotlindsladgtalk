package com.hitanshudhawan.kotlin_dsl_adg_talk

fun function() {

    a(href = "www.google.com") {
        +"Click here"
    }

}

fun a(href: String, block: Anchor.() -> Unit) {
    //...
}

class Anchor {

    operator fun String.unaryPlus() {
        //...
    }

}