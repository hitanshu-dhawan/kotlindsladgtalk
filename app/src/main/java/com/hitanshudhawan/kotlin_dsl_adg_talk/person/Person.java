package com.hitanshudhawan.kotlin_dsl_adg_talk.person;

import java.time.LocalDateTime;

public class Person {

    private String firstName;
    private String lastName;
    private LocalDateTime dateOfBirth;

    public Person(String firstName, String lastName, LocalDateTime dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    //...
}