package com.hitanshudhawan.kotlin_dsl_adg_talk

import org.json.JSONObject

val human2 = json {
    "name" too "Hitanshu"
    "age" too 22
    "male" too true
    "address" too json {
        "line1" too "Flat No. 100"
        "line2" too "Triveni Apartments"
        "city" too "New Delhi"
    }
}

fun json(block: JSONObjectBuilder.() -> Unit): JSONObject {
    return JSONObjectBuilder().apply(block).json
}

class JSONObjectBuilder {
    val json = JSONObject()

    infix fun String.too(value: Any?) {
        json.put(this, value)
    }

}